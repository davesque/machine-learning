function p = predict2(ThetaIn, ThetaHid, ThetaOut, X)
%PREDICT Predict the label of an input given a trained neural network
%   p = PREDICT(Theta1, Theta2, X) outputs the predicted label of X given the
%   trained weights of a neural network (Theta1, Theta2)

% Useful values
m = size(X, 1);
num_labels = size(ThetaOut, 1);
num_hidden_layers = size(ThetaHid, 3);

% You need to return the following variables correctly 
p = zeros(size(X, 1), 1);

hIn = sigmoid([ones(m, 1) X] * ThetaIn');

hPrev = hIn;
for i = 1:num_hidden_layers
    hPrev = sigmoid([ones(m, 1) hPrev] * ThetaHid(:, :, i)');
end

hOut = sigmoid([ones(m, 1) hPrev] * ThetaOut');

[dummy, p] = max(hOut, [], 2);

end
