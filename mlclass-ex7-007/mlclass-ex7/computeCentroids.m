function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returs the new centroids by computing the means of the 
%data points assigned to each centroid.
%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by 
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%
% Useful variables
[m n] = size(X);

% You need to return the following variables correctly.
centroids = zeros(K, n);
% ====================== YOUR CODE HERE ======================
% Instructions: Go over every centroid and compute mean of all points that
%               belong to it. Concretely, the row vector centroids(i, :)
%               should contain the mean of the data points assigned to
%               centroid i.
%
% Note: You can use a for-loop over the centroids to compute this.
%
% Convert centroid indices to 1-of-K vectors
labels = zeros(m, K);
labels(sub2ind(size(labels), 1:m, idx')) = 1;

% Rotate matrix to m-by-1-by-K form (one layer for each centroid)
labels = reshape(labels, [m 1 K]);

% Ensure a multiplier value for each sample dimension
labels = repmat(labels, [1 n 1]);

% Get counts of elements associated with each centroid
counts = sum(labels, 1);

% Convert samples to 1-by-1-by-K form (one layer for each centroid)
X = repmat(X, [1 1 K]);

% Get vector sums for each centroid's elements
sums = sum(X .* labels, 1);

% Calculate centroids
centroids = sums ./ counts;
centroids = reshape(centroids, [n K])';
% =============================================================
end
